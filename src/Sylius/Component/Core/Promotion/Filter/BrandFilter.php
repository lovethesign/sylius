<?php

namespace Sylius\Component\Core\Promotion\Filter;

use Sylius\Component\Core\Model\ProductInterface;

class BrandFilter
{

    public $brands;

    public function __construct()
    {
        $this->brands = [
                        757 => 'flos',
                        1427 => 'thesign',
                        1368 => 'seletti-wears-toiletpaper',
                        804 => 'artemide',
                        #1221 => 'menu',
                        1002 => 'fontanaarte',
                        1246 => 'muuto',
                        6039 => 'zanotta',
                        776 => 'alessi',
                        4040 => 'ethnicraft',
                        983 => 'ferm-living',
                        984 => 'fermob',
                        836 => 'bonaldo',
                        942 => 'driade',
                        4030 => 'ethimo',
                        1011 => 'fritz-hansen',
                        1094 => 'ivano-redaelli',
                        1104 => 'jv-italian-design-by-jannelli-volpi',
                        4077 => 'kartell',
                        1149 => 'le-creuset',
                        1199 => 'magis',
                        1276 => 'northern-lighting',
                        3930 => 'pyropet',
                        3931 => 'qeeboo',
                        1373 => 'sika',
                        1401 => 'string',
                        1432 => 'tolix',
                        1447 => 'universopositivo'
        ];
    }

    public function filter(array $items)
    {
        if (!isset($this->brands)) {
            return $items;
        }

        $filteredItems = [];
        foreach ($items as $item)
        {
            if ($this->notContainsBrandTaxon($item->getProduct(), $this->brands)) {
                $filteredItems[] = $item;
            }
        }

        return $filteredItems;
    }

    private function notContainsBrandTaxon(ProductInterface $product, array $brands)
    {
        foreach ($product->getTaxons() as $taxon)
        {
            if (in_array($taxon->getCode(), $brands))
            {
                return false;
            }
        }

        return true;
    }

}

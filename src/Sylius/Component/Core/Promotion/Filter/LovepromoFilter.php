<?php

namespace Sylius\Component\Core\Promotion\Filter;

use Sylius\Component\Core\Model\ProductInterface;

class LovepromoFilter
{
    CONST LOVEPROMO_TAXON = 3267;

    public function filter(array $items)
    {
        $filteredItems = [];
        foreach ($items as $item)
        {
            if ($this->notContainsLovepromoTaxon($item->getProduct())) {
                $filteredItems[] = $item;
            }
        }

        return $filteredItems;
    }

    private function notContainsLovepromoTaxon(ProductInterface $product)
    {
        foreach ($product->getTaxons() as $taxon)
        {
            if($taxon->getParent() != null)
            {
                if (LovepromoFilter::LOVEPROMO_TAXON == $taxon->getParent()->getId()) {
                    return false;
                }
            }
        }

        return true;
    }

}

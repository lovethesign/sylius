<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Sylius\Component\Core\Promotion\Action;

use Sylius\Component\Core\Distributor\IntegerDistributorInterface;
use Sylius\Component\Core\Promotion\Applicator\UnitsPromotionAdjustmentsApplicatorInterface;
use Sylius\Component\Promotion\Model\PromotionInterface;
use Sylius\Component\Promotion\Model\PromotionSubjectInterface;

/**
 * @author Paweł Jędrzejewski <pawel@sylius.org>
 * @author Saša Stamenković <umpirsky@gmail.com>
 * @author Mateusz Zalewski <mateusz.zalewski@lakion.com>
 */
class PercentageDiscountAction extends DiscountAction
{
    const TYPE = 'order_percentage_discount';

    /**
     * @var IntegerDistributorInterface
     */
    private $distributor;

    /**
     * @var UnitsPromotionAdjustmentsApplicatorInterface
     */
    private $unitsPromotionAdjustmentsApplicator;

    /**
     * @param IntegerDistributorInterface $distributor
     * @param UnitsPromotionAdjustmentsApplicatorInterface $unitsPromotionAdjustmentsApplicator
     */
    public function __construct(
        IntegerDistributorInterface $distributor,
        UnitsPromotionAdjustmentsApplicatorInterface $unitsPromotionAdjustmentsApplicator
    ) {
        $this->distributor = $distributor;
        $this->unitsPromotionAdjustmentsApplicator = $unitsPromotionAdjustmentsApplicator;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(PromotionSubjectInterface $subject, array $configuration, PromotionInterface $promotion)
    {
        $lovepromo_exceptions = ['NEW10'];

        if (!$this->isSubjectValid($subject)) {
            return;
        }

        $this->isConfigurationValid($configuration);

        $brandFilter = new \Sylius\Component\Core\Promotion\Filter\BrandFilter();
        $lovepromoFilter = new \Sylius\Component\Core\Promotion\Filter\LovepromoFilter();
        $filteredItems = $subject->getItems()->toArray();

        //$filteredItems = $brandFilter->filter($filteredItems);

        $promotion_code = $promotion->getCode() ?? '';

        if (empty($promotion_code) ||
            (!in_array($promotion_code, $lovepromo_exceptions) && 'LOVEKK' != substr($promotion_code, 0, 6) && 'LOVEFF' != substr($promotion_code, 0, 6))
        )
        {
            $filteredItems = $lovepromoFilter->filter($filteredItems);
        }

        $partialTotals = $this->getPromoPartialsTotal($filteredItems);

        $promotionAmount = $this->calculateAdjustmentAmount($partialTotals, $configuration['percentage']);
        //$promotionAmount = $this->calculateAdjustmentAmount($subject->getPromotionSubjectTotal(), $configuration['percentage']);
        if (0 === $promotionAmount) {
            return;
        }

        /*
        $splitPromotion = $this->distributor->distribute($promotionAmount, $subject->countItems());
        $this->unitsPromotionAdjustmentsApplicator->apply($subject, $promotion, $splitPromotion);
        */

        // custom split
        $splitPromotion = $this->distributor->distribute($promotionAmount, count($filteredItems));
        $this->unitsPromotionAdjustmentsApplicator->applyFiltered($filteredItems, $promotion, $splitPromotion);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurationFormType()
    {
        return 'sylius_promotion_action_percentage_discount_configuration';
    }

    /**
     * {@inheritdoc}
     */
    protected function isConfigurationValid(array $configuration)
    {
        if ($configuration['percentage'] == 0 )
            return;
        
        if (!isset($configuration['percentage']) || !is_float($configuration['percentage'])) {
            throw new \InvalidArgumentException('"percentage" must be set and must be a float.');
        }
    }

    /**
     * @param int $promotionSubjectTotal
     * @param int $percentage
     *
     * @return int
     */
    private function calculateAdjustmentAmount($promotionSubjectTotal, $percentage)
    {
        return -1 * (int) round($promotionSubjectTotal * $percentage);
    }

    private function getPromoPartialsTotal($items)
    {
        $partialTotal = 0;

        foreach ($items as $item)
        {
            $partialTotal += $item->getTotal();
        }

        return $partialTotal;
    }
}
